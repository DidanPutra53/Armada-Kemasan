import article1 from "./1.webp";
import article2 from "./2.webp";
import article3 from "./3.webp";
import article4 from "./4.webp";
import article5 from "./5.webp";
import article6 from "./6.webp";
import article7 from "./7.webp";
import map1 from "./map-1.webp";
import map2 from "./map-2.webp";
import map3 from "./map-3.webp";
import map5 from "./map-5.webp";
import map6 from "./map-6.webp";
import map7 from "./map-7.webp";
import indomap from "./indonesia-dot-map-vector-png-4.webp"

export { article1, article2, article3, article4, article5, article6, article7, map1, map2, map3, map5, map6, map7, indomap };