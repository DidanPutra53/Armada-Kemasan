import HeaderOurStory from './people-cleaning-garbage-from-nature.webp'
import banner1 from './hand-throwing-trash-wing-papers.webp';
import banner2 from './save-ocean-campaign-plastic-bag-sinking-ocean-remix-media.webp'
import banner3 from './environmentalists-recycling-world-environment-day.webp'
import OurMission from './beach-cleanup-volunteer-with-recycle-bin-save-earth-campaign.webp'
import OurImpact from './hands-cupping-recycle-save-environment-campaign.webp'
import partnership from './closeup-diverse-people-holding-hands.webp'
import partnership2 from './men-women-help-each-other-collect-garbage (1).webp'
import partnership3 from './men-women-help-each-other-collect-garbage.webp'
import investor1 from './confident-young-businessman-suit-standing-with-arms-folded.webp'
import investor2 from './handsome-businessman-suit-glasses-cross-arms-chest-look.webp'
import investor3 from './smiling-confident-businesswoman-posing-with-arms-folded.webp'
import investor4 from './professional-asian-businesswoman-gray-blazer.webp'

export { HeaderOurStory, banner1, banner2, banner3, OurMission, OurImpact, partnership, partnership2, partnership3, investor1, investor2, investor3, investor4 } 