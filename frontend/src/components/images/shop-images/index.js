import product1 from './0b668b29d2438db7d8b1a080afa28640.webp'
import product2 from './0f58beaafbb683358a026c1ecc79373a.webp'
import product3 from './6befbd169aee8815bfb0de945d5577de.webp'
import banner3 from './Banner 3.webp'
import banner2 from './Banner 2.webp'
import banner1 from './banner.webp'
import product4 from './17fd7cd8-2a90-4c6c-b9c5-6c2a073f18ec.webp'
import product5 from './36ab9f4aa750be5664f918bdfb62e70a.webp'
import product6 from './0b668b29d2438db7d8b1a080afa28640.webp'
import product7 from './37e0eb6de04f1027d06c9f80a4f3f401.webp'
import product8 from './cd1a0ceb3d83cf3f4cf30587b8c21b73.webp'
import product9 from './dbb6bcb1-e575-4b8a-a6f1-444892f23dd7.webp'
import product10 from './aac0194ac12949db4fe0eb7f84bdcdff.webp'
import product11 from './6befbd169aee8815bfb0de945d5577de.webp'
import product12 from './64b5c27b2c72e905b30a001cb3a685d2.webp'
import product13 from './79c296a3a346c7fff71d21e45c82f842.webp'
import product14 from './ac5bf1988d62495db077a0c2aaaf530a.webp'
import product15 from './e3a2f1b08edaaf71e6091e96a27c7364.webp'
import product16 from './911e0a272d9c6f2e9aa76180464ba4bc.webp'
import product17 from './c5cf41f3def4ad6be96014b1adfbb6aa.webp'
import product18 from './8d5eb9efcc46746b4d2d954805920748.webp'
import product19 from './0f58beaafbb683358a026c1ecc79373a.webp'

export {
    product1,
    product2,
    product3,
    product4,
    product5,
    product6,
    product7,
    product8,
    product9,
    product10,
    product11,
    product12,
    product13,
    product14,
    product15,
    product16,
    product17,
    product18,
    product19,
    banner2,
    banner3,
    banner1
} 